import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useLocation, useNavigate, useParams } from 'react-router-dom';

const ProductPages = ({}) => {
  const { id } = useParams();
  const [product, setproduct] = useState({})
 
  console.log(product._id)

  useEffect(()=>{
    const handleGetPRoduct = async () => {
      const res = await axios.get(`http://localhost:3001/produces/${id}`)  
      setproduct(res?.data)
      
    }
    handleGetPRoduct()
  }
    
    
    , [])
  
 
console.log(id)
  return (
    <div class="flex items-center justify-center h-screen bg-gray-400" >
    <div class="flex flex-col text-black bg-white shadow-md bg-clip-border rounded-xl w-96">
      <div class="relative mx-4 mt-4 overflow-hidden text-black bg-white bg-clip-border rounded-xl h-96">
        <img
          src={product.photo_url}
          alt="card-image"
          class="object-cover w-full h-full"
        />
      </div>
      <div class="p-6">
        <div class="flex items-center justify-between mb-2">
          <p class="block font-sans text-base antialiased font-medium leading-relaxed text-blue-gray-900">
            {product.name}
          </p>
          <p class="flex block font-sans text-green-700 antialiased font-medium leading-relaxed text-blue-gray-900">
            {product.price}
            <p>$</p>
          </p>
        </div>
        <p class="block font-sans text-sm antialiased font-normal leading-normal text-gray-700 opacity-75">
          {product.informacion}
        </p>
        <p class="block font-sans text-sm antialiased font-normal leading-normal text-gray-900 opacity-75">
          <p>Serie: </p>
          {product.serie}
        </p>
      </div>
      <div class="p-6 pt-0">
        <button
          class="align-middle select-none font-sans font-bold text-center uppercase transition-all disabled:opacity-50 disabled:shadow-none disabled:pointer-events-none text-xs py-3 px-6 rounded-lg shadow-gray-900/10 hover:shadow-gray-900/20 focus:opacity-[0.85] active:opacity-[0.85] active:shadow-none block w-full bg-blue-gray-900/10 text-blue-gray-900 shadow-none hover:scale-105 hover:shadow-none focus:scale-105 focus:shadow-none active:scale-100"
          type="button"
        >
          Add to Cart
        </button>
      </div>
    </div>
  </div>
  
  )
}

export default ProductPages