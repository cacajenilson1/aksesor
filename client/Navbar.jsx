import React, { useState, useEffect,  } from 'react';
import { Link } from 'react-router-dom';
import { useNavigate } from "react-router-dom";
import { useCookies } from "react-cookie";
import { FaStoreAlt } from "react-icons/fa";
import { LuLogOut } from "react-icons/lu";
import './navbar.css';
const Navbar = (props) => {
    
  
    const [email, setEmail] = useState(window.localStorage.getItem("email"));
    const [cookies, setCookies] = useCookies(["email"]);

    const navigate = useNavigate();
    
    const isLoggedin = props.isLoggedin

    console.log(isLoggedin, 'isloggedin from navbar')
    const handleLogOut = () => {
        props.setLoggedIn(false)
        // localStorage.removeItem('isLoggedIn')
        // window.localStorage.clear();
        navigate("/home");
    }

    return (
        <>
            <div className='text-black bg-gradient-to-t from-gray-300 via-gray-500 to-gray-900 flex justify-between p-5  '>
                <div className="w-[100px] "><div > <FaStoreAlt size={40}/></div></div>
                <div className="flex justify-around w-[55%] text-[20px] items-center">
                    <Link to='/home'>Home</Link>
                    <Link to='/about'>About</Link>
                    {
                        isLoggedin ?
                            <>
                               
                                
                                <Link to='/setproduct'>Set Product</Link>
                                
                                <button type="button" className="Btni" onClick={handleLogOut}>
                                <div className='sing'> < LuLogOut /> </div>
                                <div class="text">Logout</div>
                                </button>
                            </>
                            :
                            <>
                                <Link to='/login'>Log in</Link>
                                <Link to='/singup'>Sing up</Link>
                            </>
                    }
                </div>
            </div>
        </>
    )
}

export default Navbar