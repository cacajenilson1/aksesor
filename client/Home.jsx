import React, {useEffect, useState} from 'react';
import axios from 'axios';
import Box from '../../compnent/Box';

import './Home.css'
const Home = () => {
  const [ produces, setProduces ] = useState([]);
console.log(produces)
  useEffect(() => {
    function fetchData() {
      axios.get('http://localhost:3001/produces/')
        .then(function (response) {
          return response.data
        })
        .then(data => {
          setProduces(currentProduces => [...produces, ...data]);
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    fetchData();
  }, []);
  
  return (
    <div className='w-full flex justify-center bg-gray-300 '>
      <div className='w-full h-full p-10 grid grid-cols-4 gap-x-2	gap-y-10	'>
          {
            produces.map(produces=>{
              return (
                <Box key={produces._id} produce_id={produces._id} name={produces.name} informacion={produces.informacion} serie={produces.serie} price={produces.price} photo_url={produces.photo_url} /> 
              )
            })
          }
      </div>
    </div>
  )
}

export default Home