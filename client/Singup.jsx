import React, { useState } from 'react'
import axios from 'axios'
import { useNavigate } from "react-router-dom"
import { useCookies } from "react-cookie"
import './singup.css'
const Singup = () => {
    const [_, setCookies] = useCookies(["email"]);

    const [name, setName] = useState("");
    const [last_name, setLast_name] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [cpassword, setConfigpassword] = useState("");

    const navigate = useNavigate();

    const handleSubmit = (event) => {
        event.preventDefault();
        if (email == '' || password == '') {
            return
        }
        //kerkes per ne back
        axios.post('http://localhost:3001/auth/register',{
            name: name,
            last_name: last_name,
            email: email,
            password : password,
            
        })
        .then(function (response) {
            setCookies("token", response.token);
          })
          .catch(function (error) {
            console.log(error);
          });
          

        setCookies("email", email);

        
        window.localStorage.setItem("email", email);
        navigate("/login");
    }
    
  return (
    <div className='background-color: rgb(100 116 139) p-[20px] pb-[30px]'>
    <div className='flex justify-center'>
        <form className="wrapper">
            <h2>SINGUP</h2>
            <section className="group">
                <input
                    type="text"
                    size="30"
                    className="input"
                    name="name"
                    required
                    onChange={(event)=> setName(event.target.value)}
                />
                <label htmlFor="email" className="label">
                    Name
                </label>
            </section>
            <section className="group">
                <input
                    type="text"
                    size="30"
                    className="input"
                    name="last_name"
                    required
                    onChange={(event)=> setLast_name(event.target.value)}
                />
                <label htmlFor="last_name" className="label">
                    Last Name
                </label>
            </section>
            <section className="group">
                <input
                    type="text"
                    size="30"
                    className="input"
                    name="email"
                    required
                    onChange={(event)=> setEmail(event.target.value)}
                />
                <label htmlFor="email" className="label">
                    Email
                </label>
            </section>
            <section className="group">
                <input
                    type="password"
                    minLength="8"
                    className="input"
                    name="password"
                    required
                    onChange={(event)=> setPassword(event.target.value)}
                />
                <label htmlFor="password" className="label">
                    Password
                </label>
            </section>
            <section className="group">
                <input
                    type="password"
                    minLength="8"
                    className="input"
                    name="cpassword"
                    required
                    onChange={(event)=> setConfigpassword(event.target.value)}
                />
                <label htmlFor="cpassword" className="label">
                    Confirm Password
                </label>
            </section>
            <button type="button" className="btn" onClick={handleSubmit}>
                SINGUP
            </button>
            <span className="footer"></span>
        </form>
    </div>
</div> 
  )
}

export default Singup