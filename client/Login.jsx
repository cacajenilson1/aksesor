import React, { useState } from 'react'
import { useNavigate } from "react-router-dom"
import axios from 'axios';
import { useCookies } from "react-cookie"
import './login.css'

const Login = ({setLoggedIn,setUser}) => {
    const [_, setCookies] = useCookies(["email"]);
    
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    console.log(password)

    const navigate = useNavigate();

    const handleSubmit = (event) => {
        event.preventDefault();
        if (email == '' || password == '') {
            return
        }
  //kerkes per ne back
  axios.post('http://localhost:3001/auth/login',{
    email: email,
    password : password
    
})
.then(function (response) {
    if(response.data.user){

        localStorage.setItem('isLoggedIn', true)
        console.log("inside axios success callback");
        setLoggedIn(true)
        setUser(response.data.user)
        
    }
   
    navigate("/home");
  })
  .catch(function (error) {
    alert("kredencjale te gabuara");
    console.log(error);
  });
  
        

        
       
        
    }

    return (
        <div className='bg  p-[20px] pb-[30px]'>
            <div className='flex justify-center'>
                <form className="wrapper">
                    <h2>LOGIN</h2>
                    <section className="group">
                        <input
                            type="text"
                            size="30"
                            className="input"
                            name="email"
                            required
                            onChange={(event)=> setEmail(event.target.value)}
                        />
                        <label htmlFor="email" className="label">
                            Email
                        </label>
                    </section>
                    <section className="group">
                        <input
                            type="password"
                            minLength="8"
                            className="input"
                            name="password"
                            required
                            onChange={(event)=> setPassword(event.target.value)}
                        />
                        <label htmlFor="password" className="label">
                            Password
                        </label>
                    </section>

                    <button type="button" className="btn" onClick={handleSubmit}>
                        LOGIN
                    </button>
                    <span className="footer"></span>
                </form>
            </div>
        </div> 
    );
}

export default Login;
