import React, { useState } from 'react'
import axios from 'axios';
import { useCookies } from "react-cookie";
import { useJwt } from "react-jwt";
import { useNavigate } from "react-router-dom";

const Box = ({produce_id, name, serie, price , informacion, photo_url}) => {
    const [saved, setSaved] = useState(false);
    const [cookies, setCookies] = useCookies(["name"]);
    const { decodedToken, isExpired } = useJwt(cookies.token);
    const navigator = useNavigate();

    const handleNavigate = (id) => {
      // Navigate to the '/singleProduct/:id' route with the provided ID
      navigator(`/singleProduct/${id}`);
    };

    function handleSaveRecipe() {
        setSaved(true);
        axios.put('http://localhost:3001/recipes/saveRecipe',{
            prduct_ID: produce_id,
            userID: decodedToken.user._id,
        })
    }

    function handleClick() {
       handleNavigate(produce_id)
    }

    

    return (
        



        <div onClick={handleClick} className='flex justify-center'>
        <div style={{maxWidth:'20rem'}} className="max-w-sm rounded-lg overflow-hidden shadow-lg bg-white">
            <img className="w-full" src={photo_url} alt="Sunset in the mountains"/>
                <div className="px-6 py-4">
                    <div className="flex justify-between font-bold text-xl mb-2">
                        <div>{name}</div>
                        <div className='flex text-green-700'>{price}<p>$</p></div>
                       
                           
                    </div>
                    <p className="text-gray-700 text-base">
                        {serie}
                    </p>
                </div>
                <div className="px-6 pt-4 pb-2">
                   {informacion}
                </div>
        </div>
    </div>
    )
}

export default Box