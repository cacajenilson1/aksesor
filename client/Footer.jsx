import React ,{ useState, useEffect,  } from 'react';
import { Link } from 'react-router-dom';
import { useNavigate } from "react-router-dom";
import { useCookies } from "react-cookie";
import { FaInstagram } from "react-icons/fa6";
import { CiFacebook } from "react-icons/ci";
import { MdOutlineMail } from "react-icons/md";
import { LuLogOut } from "react-icons/lu";

const Footer = (props) => {
  const [email, setEmail] = useState(window.localStorage.getItem("email"));
  const [cookies, setCookies] = useCookies(["email"]);

  const navigate = useNavigate();
  
  const isLoggedin = props.isLoggedin

  console.log(isLoggedin, 'isloggedin from navbar')
  const handleLogOut = () => {
      props.setLoggedIn(false)
      // localStorage.removeItem('isLoggedIn')
      // window.localStorage.clear();
      navigate("/login");
  }
  return (
    <div className='text-gray-1000 bg-gradient-to-t from-gray-900 via-gray-500 to-gray-300 flex justify-between p-5  '>
    <div className=' inline flex-col justify-around text-[20px] items-center '> 
        
       <div> <Link to='/home'>Home</Link></div>
       <div><Link to='/about'>About</Link></div>
       {
                        isLoggedin ?
                            <>
                               
                                
                                <div><Link to='/setproduct'>Set Product</Link></div>
                                
                                <button type="button " className="border-none" onClick={handleLogOut}>
                               
                                Logout
                                </button>
                            </>
                            :
                            <>
                                <div><Link to='/login'>Log in</Link></div>
                                
                                <Link to='/singup'>Sing up</Link>
                            </>
                    }
    </div>
    <div className='flex'>
    <div className='flex flex-col justify-around w[50%]  text-[20px] '>
        <div><FaInstagram/></div>
        <div><CiFacebook /></div>
        <div><MdOutlineMail /></div>
    </div>
    <div className='flex flex-col justify-around w[50%]  text-[20px] items-center'>
        <div>Auto_online_Xh&Xh</div>
        <div>Auto XH&XH</div>
        <div>Autoxhxh@gmail.com</div>
    </div>
    </div>
    <div className=''>
        <h1>ADRESS:</h1>
        <div>Tiran</div>
        <div>Fier</div>
        <div>Shkoder</div>
    </div>
    </div>
  )
}

export default Footer