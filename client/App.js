import { Route, Routes } from 'react-router-dom';


import Home from './pages/home/Home';
import About from './pages/about/About';
import Login from './pages/login/Login';
import Footer from './compnent/Footer';
import Navbar from './compnent/Navbar';
import Singup from './pages/singup/Singup';


import Setproduct from './pages/Setproduct/Setproduct';
import { useEffect, useState } from 'react';
import ProductPages from './pages/ProductPages/ProductPages';
function App() {

 const [loggedIn, setLoggedIn] = useState(localStorage.getItem('isLoggedIn'));
 const [user, setUser] = useState({});
 console.log(user,'user');

  return (
    <>
   
      
    
    <Navbar isLoggedin={loggedIn} setLoggedIn={setLoggedIn}/>
    <Routes>
    <Route path='/' element={<Login/>}></Route>
      <Route path='/home' element={<Home/>}/>
      <Route path='/about' element={<About/>}  />
      <Route path='/login' element={<Login setLoggedIn={setLoggedIn} setUser={setUser}/>}  />
      <Route path='/singup' element={<Singup/>}/>
      <Route path='/singleProduct/:id' element={<ProductPages/>} />
      <Route path='/setproduct' element={<Setproduct user={user}/>}/>
    </Routes>
    <Footer isLoggedin={loggedIn} setLoggedIn={setLoggedIn}/>
    </>
    
  );
}

export default App;
