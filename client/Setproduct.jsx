  import React, { useState } from 'react'
  import { useCookies} from "react-cookie";
  import {useNavigate} from "react-router-dom";
  import { useJwt } from "react-jwt";
  import axios from "axios";
  const Setproduct = ({ user }) => { 
    console.log(user)
    const [cookies, setCookies] = useCookies(["name"]);
    // const { decodedToken, isExpired } = useJwt(cookies.token);
    const navigator = useNavigate();
    const [product, setProduct] = useState({
    name: "",
    serie: "",
    price: 0,
    photo_url: "",
    info: "",
   });
   const handleChange = (event) => {
    const { name, value } = event.target;
    setProduct({ ...product, [name]: value });
};


   const handelSet = () =>{
    //kerkes per ne back
  axios.post('http://localhost:3001/produces/',{
    name: product.name,
    serie: product.serie,
    price:product.price,
    photo_url: product.photo_url,
    informacion: product.info,
    useOwner: user._id,
    
    
})
.then(function (response) {
    console.log("inside axios success callback");
    console.log(response.data.data);
    if(response.data.data){
        setCookies("token", response.data);
    }
    navigator("/home");
    
  
    
  })
  .catch(function (error) {
    alert("kredencjale te gabuara");
    console.log(error);
  });
  
   }

    return (
      <div className='bg-gray-400   p-[20px] pb-[30px]'>
      <div className='flex justify-center'>
          <form className="wrapper">
            <h2>SET NEW PRODUCT</h2>
              <section className="group">
                  <input
                      type="text"
                      size="30"
                      className="input"
                      name="name"
                      required
                      onChange={handleChange}
                  />
                  <label htmlFor="name" className="label">
                      Name
                  </label>
              </section>
              <section className="group">
                  <input
                      type="text"
                      minLength="8"
                      className="input"
                      name="serie"
                      required
                      onChange={handleChange}
                    
                  />
                  <label htmlFor="serie" className="label">
                      Seria
                  </label>
              </section>
              <section className="group">
                  <input
                      type="number"
                      minLength="8"
                      className="input"
                      name="price"
                      required
                      onChange={handleChange}
                    
                  />
                  <label htmlFor="price" className="label">
                      price( $ )
                  </label>
              </section>
              <section className="group">
                  <input
                      type="text"
                      minLength="8"
                      className="input"
                      name="photo_url"
                      required
                      onChange={handleChange}
                  />
                  <label htmlFor="photo_url" className="label">
                      Photo URL
                  </label>
              </section>
              <section className="group">
                  <textarea
                      type="text"
                      className="input"
                      name="info"
                      required
                      onChange={handleChange}
                    
                  />
                  <label htmlFor="info" className="label">
                      Informacion
                  </label>
              </section>
              <button type="button" className="btn" onClick={handelSet} >
                  SHTO
              </button>
              <span className="footer"></span>
          </form>
      </div>
  </div> 
    )
  }

  export default Setproduct