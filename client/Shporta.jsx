import React, { useState, useEffect } from 'react'
import axios from 'axios';
import { useLocation } from "react-router-dom";
import Setproduct from '../pages/Setproduct/Setproduct';

const Shporta = () => {
    const { state } = useLocation();
    const [product_id, setproduct_id] = useState(state.product_id);
    const [product, setproduct] = useState();
  
    useEffect(() => {
        function fetchData() {
            axios.get(`http://localhost:3001/recipes/${product_id}`)
            .then(function (response) {
                return response.data;
            })
            .then(data => {
                setproduct(data);
            })
            .catch(function (error) {
                console.log(error);
            });
        }

        fetchData();
    }, [])
    

    return (
        <div>
            {
                product._id
            }
        </div>
    )
}

export default Shporta