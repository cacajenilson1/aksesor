import React, { useState } from 'react'
import { useNavigate } from "react-router-dom";

import './auth.css'


const Auth = () => {
    const [email, setEmail] = useState("");
    const [pass, setPassword] = useState("");
    const navigate = useNavigate();

    const handleLogOut = () => {
        window.localStorage.setItem("email", "");
        navigate("/auth");
    }

    if (window.localStorage.getItem("email") !== '') {
        return(
            <>
                <h1>Je bere log in</h1>
                <button type="button" className="btn" onClick={handleLogOut}>
                    LOGOUT
                </button>
            </>
        )
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        window.localStorage.setItem("email", email)
        navigate("/auth");
    }

    return (
        <div className='flex justify-center p-5 pb-[20%]'>
            <form className="wrapper">
                <h2>LOGIN</h2>
               
                <section className="group">
                    <input
                        type="text"
                        size="30"
                        className="input"
                        name="email"
                        required
                        onChange={(event) => setEmail(event.target.value)}
                    />
                    <label htmlFor="email" className="label">
                        Email
                    </label>
                </section>
                <section className="group">
                    <input
                        type="password"
                        minLength="8"
                        className="input"
                        name="password"
                        required
                        onChange={(event) => setPassword(event.target.value)}
                    />
                    <label htmlFor="password" className="label">
                        Password
                    </label>
                </section>
                <button type="button" className="btn" onClick={handleSubmit}>
                    LOGIN
                </button>
                <span className="form_shadow"></span>
            </form>
        </div>
    )
}

export default Auth