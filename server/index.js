const express = require('express');
const cors= require("cors");
const mongoose =require("mongoose");
const morgan = require("morgan");
const app = express();
const userRouter = require("./routes/users");
const producesRouter = require("./routes/produces");

app.use(express.json());
app.use(cors());
app.use(morgan('short'));
 
app.use("/auth", userRouter);
app.use("/produces", producesRouter);

mongoose.connect(
    'mongodb+srv://cacajenilson1:QCCMmnoKlbEfxGK8@cluster0.xbhrekr.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0'
);
app.listen(3001, () => console.log("server start"));
