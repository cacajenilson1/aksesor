const mongoose = require ("mongoose");

const UserSchema = new mongoose.Schema({
    email: { type: String, required: true, unique: true },
    //password2: { type: String, required: true }, 
    name: { type: String, required: true },
    last_name: { type: String, required: true },
    password: { type: String, required: true }, 
    savedProduces: [
        { type: mongoose.Schema.Types.ObjectId, ref: "Produces" }
      ],
});


const UserModel = mongoose.model("users", UserSchema);

module.exports = UserModel;