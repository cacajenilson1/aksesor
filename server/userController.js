const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');
const router = express.Router();

const UserModel = require('../models/User');
const auth_messages = require('../constants/auth_messages')

const userRegister =  async (req, res) =>{  
    const {
        name,
        last_name,
        email,
        password,
       
      
    } = req.body;
    
    if(email.length <= 5){
        return res.status(403).json({
            message:auth_messages.invalid_email,
        });
    }
    
    const user = await UserModel.findOne({ password });
    if ( user){
        return res.status(403).json({
            message:auth_messages.duplicated_entry,
        });
        
    }
    const User = new UserModel({
         name,
         last_name,
         email,
        password,
       
    })
   
    await User.save();
    const token = jwt.sign({ User }, "secret");
    return res.status(200).json({
        message:auth_messages.succesfully_created,
        token: token
    });
};

const userLogIn =  async (req, res) =>{  
   const { email , password }= req.body;
   //const isPasswordValid = await bcrypt.compare(password, user.password);
   const newUser = await UserModel.findOne({ email , password });
   if(newUser){
    
      return res.status(200).json({
            message:auth_messages.succesfully_authenticated ,
            user:newUser

       });
    
   }else{
    return res.status(401).json({
        message: auth_messages.user_does_not_exist,
    })
   }



};

module.exports =  {
    userRegister,
    userLogIn
}
