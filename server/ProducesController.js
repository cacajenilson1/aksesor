const ProducesModel = require('../models/Produces');
const UserModel = require('../models/User');

const getAllProduces = async (req, res) => {
    try {
        const result = await ProducesModel.find({});
        res.status(200).json(result);
        
    } catch (error) {
        res.status(500).json(error);
        
    }
}
  
const addNewProduces = async (req, res) => {

    const produce = new ProducesModel({
        name: req.body.name,
        serie: req.body.serie,
        price: req.body.price,
        photo_url: req.body.photo_url,
        informacion: req.body.informacion,
        useOwner: req.body.useOwner,
    });
    console.log(produce);
    try {
        const result = await produce.save();
        res.status(201).json({
            createdProduces: {
                name: result.name,
                serie: result.serie,
                price: result.price,
                photo_url: result.photo_url,
                informacion: result.informacion,
                _id: result._id,
            },
        });
      } catch (err) {
        res.status(500).json(err);
        console.log("errori ne ruajtje");
}}


const getProduceById = async (req, res) => {
    try {
      const result = await ProducesModel.findById(req.params.produceId);
      res.status(200).json(result);
    } catch (err) {
      res.status(500).json(err);
    }
  }



module.exports = {
    getAllProduces,
    addNewProduces,
    getProduceById,
    
}