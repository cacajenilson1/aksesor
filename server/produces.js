const express = require('express');
const router = express.Router();

const {
    getAllProduces,
    addNewProduces,
    getProduceById,
    getSavedProduces,
    saveProduct,
    getIdSavedProduces

} = require('../controller/ProducesController');

router.get("/", getAllProduces);

router.post("/", addNewProduces);

router.get("/:produceId", getProduceById,);





module.exports = router;
