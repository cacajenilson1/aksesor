const mongoose = require("mongoose");

const ProduSchema = new mongoose.Schema({
    name: { type: String, required: true },
    serie: { type: String, required: true },
    price: { type: Number, required: true },
    photo_url: { type: String, required: true },
    informacion: { type: String, required: true },
    useOwner: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User", 
        required: true,
    },
});

const ProducesModel = mongoose.model("Produces", ProduSchema);

module.exports = ProducesModel;
